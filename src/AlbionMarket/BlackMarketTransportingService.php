<?php

declare(strict_types=1);

namespace MZierdt\Albion\AlbionMarket;

use MZierdt\Albion\Entity\AdvancedEntities\BlackMarketTransportEntity;
use MZierdt\Albion\Entity\ItemEntity;

class BlackMarketTransportingService extends Market
{
    public function __construct()
    {
    }

    public function calculateCityItem(ItemEntity $bmItem, array $Items): ItemEntity
    {
        /** @var ItemEntity $item */
        foreach ($Items as $item) {
            if ($item->getTier() === $bmItem->getTier() &&
                $item->getName() === $bmItem->getName()) {
                return $item;
            }
        }
        throw new \RuntimeException('No Item found for ' . $bmItem->getName());
    }

    public function calculateAmount(int $primAmount, int $secAmount, array $amountConfig): int
    {
        $totalAmount = $primAmount + $secAmount;
        return $amountConfig[$totalAmount];
    }

    public function calculateBmtEntity(
        BlackMarketTransportEntity $bmtEntity,
        array $cityItems,
        $amountConfig1,
        string $city
    ): BlackMarketTransportEntity {
        $bmItem = $bmtEntity->getBmItem();
        $bmtEntity->setCityItem($this->calculateCityItem($bmItem, $cityItems));
        $cityItem = $bmtEntity->getCityItem();

        $bmtEntity->setAmount(
            $this->calculateAmount(
                $cityItem->getPrimaryResourceAmount(),
                $cityItem->getSecondaryResourceAmount(),
                $amountConfig1
            )
        );

        $bmtEntity->setComplete(
            $this->isComplete(
                [$bmItem->getSellOrderPrice(), $cityItem->getSellOrderPrice(), $cityItem->getBuyOrderPrice()]
            )
        );
        $bmtEntity->setCity($city);
        return $bmtEntity;
    }

    public function calculateProfitByBmtEntity(BlackMarketTransportEntity $bmtEntity): BlackMarketTransportEntity
    {
        $cityItem = $bmtEntity->getCityItem();
        $bmItem = $bmtEntity->getBmItem();

        $bmtEntity->setMaterialCostSell($cityItem->getSellOrderPrice());
        $bmtEntity->setProfitSell(
            $this->calculateProfit($bmItem->getSellOrderPrice(), (int) $bmtEntity->getMaterialCostSell())
        );
        $bmtEntity->setProfitPercentageSell(
            $this->calculateProfitPercentage($bmItem->getSellOrderPrice(), $cityItem->getSellOrderPrice())
        );
        $bmtEntity->setProfitGradeSell($this->calculateProfitGrade($bmtEntity->getProfitPercentageSell()));

        $cityItemPrice = $this->calculateBuyOrder($cityItem->getBuyOrderPrice());
        $bmtEntity->setMaterialCostBuy($this->calculateBuyOrder($cityItemPrice));
        $bmtEntity->setProfitBuy(
            $this->calculateProfit($bmItem->getSellOrderPrice(), (int) $bmtEntity->getMaterialCostBuy())
        );
        $bmtEntity->setProfitPercentageBuy(
            $this->calculateProfitPercentage($bmItem->getBuyOrderPrice(), $cityItemPrice)
        );
        $bmtEntity->setProfitGradeBuy($this->calculateProfitGrade($bmtEntity->getProfitPercentageBuy()));
        return $bmtEntity;
    }
}
