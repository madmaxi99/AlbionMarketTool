<?php

declare(strict_types=1);

namespace MZierdt\Albion\commands;

use MZierdt\Albion\repositories\ItemRepository;
use MZierdt\Albion\repositories\JournalRepository;
use MZierdt\Albion\repositories\MaterialRepository;
use MZierdt\Albion\repositories\ResourceRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'albion-helper:clearDatabase', description: 'Clear complete database')]
class HelperPruneCommand extends Command
{
    public function __construct(
        protected ItemRepository $itemRepository,
        protected ResourceRepository $resourceRepository,
        protected MaterialRepository $materialRepository,
        protected JournalRepository $journalRepository,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Cleanup Database');

        $this->itemRepository->prune();
        $this->resourceRepository->prune();
        $this->materialRepository->prune();
        $this->journalRepository->prune();

        $output->writeln('Cleared.');
        return self::SUCCESS;
    }
}
