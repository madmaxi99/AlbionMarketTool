<?php

declare(strict_types=1);

namespace MZierdt\Albion\repositories;

use MZierdt\Albion\Entity\MaterialEntity;

class MaterialRepository extends Repository
{
    public function prune(): void
    {
        $this->pruneGeneral('materials');
    }

    public function createOrUpdate(MaterialEntity $materialEntity): void
    {
        $oldMaterialEntity = $this->entityManager
            ->getRepository(MaterialEntity::class)
            ->findOneBy(
                [
                    'tier' => $materialEntity->getTier(),
                    'name' => $materialEntity->getName(),
                    'city' => $materialEntity->getCity(),
                ]
            );

        $this->updatePrices($materialEntity, $oldMaterialEntity);
    }

    public function getMaterialsByLocation(string $city): array
    {
        return $this->findBy(MaterialEntity::class, ['city' => $city]) ?? [];
    }

    public function getHeartsAndSigilsByCity(string $city): array
    {
        return $this->findBy(MaterialEntity::class, ['city' => $city, 'type' => 'heartsAndSigils']) ?? [];
    }

    public function getCapeArtifactsByCity(string $city): array
    {
        return $this->findBy(MaterialEntity::class, ['city' => $city, 'type' => 'capeArtifacts']) ?? [];
    }

    public function getTomesByCity(string $city): ?MaterialEntity
    {
        $tomes = $this->findBy(MaterialEntity::class, ['city' => $city, 'type' => 'tomes']);
        return $tomes[0];
    }
}
