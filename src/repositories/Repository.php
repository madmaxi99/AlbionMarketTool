<?php

declare(strict_types=1);

namespace MZierdt\Albion\repositories;

use Doctrine\ORM\EntityManager;
use MZierdt\Albion\Entity\ItemEntity;
use MZierdt\Albion\Entity\JournalEntity;
use MZierdt\Albion\Entity\MaterialEntity;
use MZierdt\Albion\Entity\ResourceEntity;

class Repository
{
    public function __construct(
        protected readonly EntityManager $entityManager
    ) {
    }

    public function pruneGeneral(string $table): void
    {
        $qb = $this->entityManager
            ->getConnection()
            ->createQueryBuilder()
            ->delete($table);
        $qb->executeStatement();
    }

    public function update(mixed $entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);
    }

    public function findBy(string $class, array $params, array $sort = []): ?array
    {
        return $this->entityManager->getRepository($class)
            ->findBy($params, $sort);
    }

    public function delete(mixed $entity): void
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush($entity);
    }

    protected function updateClass(mixed $entity, mixed $oldEntity)
    {
        $entity->setId($oldEntity->getId());

        return $oldEntity;
    }

    protected function updatePrices(
        ItemEntity|ResourceEntity|MaterialEntity|JournalEntity $entity,
        ItemEntity|ResourceEntity|MaterialEntity|JournalEntity|null $oldEntity
    ): void {
        if ($oldEntity !== null) {
            if ($entity->getSellOrderPrice() !== 0) {
                $oldEntity->setSellOrderPrice($entity->getSellOrderPrice());
                $oldEntity->setSellOrderDate($entity->getSellOrderDate());
            }
            if ($entity->getBuyOrderPrice() !== 0) {
                $oldEntity->setBuyOrderPrice($entity->getBuyOrderPrice());
                $oldEntity->setBuyOrderDate($entity->getBuyOrderDate());
            }
            $this->update($oldEntity);
        } else {
            $this->update($entity);
        }
    }
}
