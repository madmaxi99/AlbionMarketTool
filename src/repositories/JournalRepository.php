<?php

declare(strict_types=1);

namespace MZierdt\Albion\repositories;

use MZierdt\Albion\Entity\JournalEntity;

class JournalRepository extends Repository
{
    public function prune(): void
    {
        $this->pruneGeneral('journals');
    }

    public function createOrUpdate(JournalEntity $journalEntity): void
    {
        $oldJournalEntity = $this->entityManager
            ->getRepository(JournalEntity::class)
            ->findOneBy(
                [
                    'tier' => $journalEntity->getTier(),
                    'name' => $journalEntity->getName(),
                    'city' => $journalEntity->getCity(),
                ]
            );

        $this->updatePrices($journalEntity, $oldJournalEntity);
    }

    public function getJournalsFromCity(string $city): array
    {
        return $this->findBy(JournalEntity::class, ['city' => $city]) ?? [];
    }
}
