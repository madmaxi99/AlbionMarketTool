<?php

declare(strict_types=1);

namespace MZierdt\Albion\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use MZierdt\Albion\AlbionMarket\NoSpecCraftingService;
use MZierdt\Albion\repositories\AdvancedRepository\NoSpecCraftingRepository;
use MZierdt\Albion\Service\TimeService;
use Twig\Environment;

class NoSpecCraftingHandler
{
    public function __construct(
        private readonly Environment $twigEnvironment,
        private readonly NoSpecCraftingRepository $noSpecRepository,
        private readonly NoSpecCraftingService $noSpecCraftingService,
    ) {
    }

    public function handler(): HtmlResponse
    {
        $cityData = [];
        $alertMessage = null;
        if (! empty($_GET)) {
            $city = $_GET['itemCity'];
            try {
                $cityData = $this->noSpecRepository->getAllNoSpecCraftingByCity($city);
            } catch (\Exception $exception) {
                $alertMessage = $exception->getMessage();
            }

            foreach ($cityData as $noSpecEntity) {
                $this->noSpecCraftingService->calculateProfitByNoSpecEntity($noSpecEntity);
            }
        }

        $htmlContent = $this->twigEnvironment->render(
            'NoSpecCrafting.html.twig',
            [
                'dataArray' => $cityData,
                'alertMessage' => $alertMessage,
                'timeThreshold' => TimeService::getFiveDaysAgo(new \DateTimeImmutable()),
            ]
        );
        return new HtmlResponse($htmlContent);
    }
}
