# AlbionMarketTool

Ziel: Eint Tool zur Marktanalyse von Albion Online

## Start
```shell
eval `ssh-agent`
. ./alias.sh
dc up -d
```

[Access AlbionMarketTool](http://localhost:8080)
[Access DB](http://localhost:8081)

## Composer pakete installieren

```shell
composer up

```

## Jobs

```shell script
rcli update:items  # update items 
rcli update:resources  # update resource 
rcli update:journals  # update journal
rcli update:materials  # update materials
```

## Composer

```shell script
rcomposer up 
rcomposer ecs # ecs-fix
rcomposer stan
rcomposer rector
rcomposer phpunit
```

## Team

[private](https://confluence.mehrkanal.com/#recently-worked)

## Changelogger

```shell
changelogger new -u
changelogger release <tag>
changelogger clean
```

## ToDO

* ~~Tier überarbeiten~~
* Farming
* ~~Revisit Profit Quotient -> Profit Percentage~~
* Javascript überall
* ~~No Spec Crafting~~
  * ~~Capes (028)~~
  * ~~Royal Items~~
  * ~~Enchanting (and/or BM)~~
* Stone
* ~~Bags BM Crafting / Capes~~
* Rework That there is only One Item with x- Prices
* ~~Add royal sigil enchanting~~
* add Artifact crafting
* ~~Bug: Age is not correct~~
* ~~Split up Items CronJobs~~
* Update Profit Quotient Grading
* Show all Transportable Items and their respective City
* Codeception
* ~~add 10% and 20% Crafting Bonus~~
* Luxury Goods
* Amount for RoyalItesm /Capes
* No Buy Order security ( 10% from sellOrder)
* Hovering over (BLack Market Resource or No SPec shows dropdown)
* Bug Enchanting
* ~~Command Cleanup~~
* Command rework
* ~~null entities in db~~
* bug mit transmutation
* time for when last updated
* ~~update amount depending on weight also profit should be total profit~~
* show better timing for prices
* ~~show bm sells aswell~~
* crafting handler aswell
* ~~single resource allow Null~~

## Plans for *0.12.0*

* crafting Fee
* Tier select
* tier select in crafting
* Tier Select in Enchanting
* maybe use min max for tier Select
* only sort by tier name weapongroup
* show whats missing for completion ( maybe a marker for when journals are missing)
