$(function() {
  let sellOrder = true;
  var minEl = $('.min');
  var maxEl = $('.max');

  // Custom range filtering function
  $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
    var min = parseInt(minEl.val(), 10);
    var max = parseInt(maxEl.val(), 10);
    var age = parseFloat(data[0]) || 0; // use data for the age column

    if (
        (isNaN(min) && isNaN(max)) ||
        (isNaN(min) && age <= max) ||
        (min <= age && isNaN(max)) ||
        (min <= age && age <= max)
    ) {
      return true;
    }

    return false;
  });

  var table = $('.sortTable').DataTable({'pageLength': 50});

  // Changes to the inputs will trigger a redraw to update the table
  minEl.on('input', function() {
    console.log('minEl');
    table.draw();
  });
  maxEl.on('input', function() {
    console.log('minEl');
    table.draw();
  });

  $('#toggleSellBuy').on('click', function() {
    $('#resourceSell').toggle();
    $('#resourceBuy').toggle();
    sellOrder = !sellOrder;
  });

  $('#myModal').on('shown.bs.modal', function() {
    $('#myInput').trigger('focus');
  });

  // $('.tierSelect').on('change', function(e) {
  //   let tier = $(this).val();
  //   if (sellOrder) {
  //     $('.sell' + tier).toggle();
  //     console.log('1');
  //   } else {
  //     $('.buy' + tier).toggle();
  //     console.log('2');
  //   }
  //   console.log('.sell' + tier);
  // });

});